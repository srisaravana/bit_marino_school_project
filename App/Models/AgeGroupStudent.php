<?php


namespace App\Models;


use App\Core\Database\Database;
use PDO;

class AgeGroupStudent implements IModel
{

    public const TABLE = "age_group_students";

    public ?int $id, $student_id, $age_group_id;

    public ?Student $student;
    public ?SportAgeGroup $sportAgeGroup;


    public static function build( $array ): self
    {
        $object = new self();
        foreach ( $array as $key => $value ) {
            $object->$key = $value;
        }
        return $object;
    }

    public static function find( int $id )
    {
        /** @var self $result */
        $result = Database::find( self::TABLE, $id, self::class );

        if ( !empty( $result ) ) {
            $result->student = Student::find( $result->student_id );
            $result->sportAgeGroup = SportAgeGroup::find( $result->age_group_id );
        }

    }

    public static function findAll( $limit = 1000, $offset = 0 )
    {
        // TODO: Implement findAll() method.
    }


    public function insert(): int
    {
        $data = [
            "age_group_id" => $this->age_group_id,
            "student_id" => $this->student_id,
        ];
        return Database::insert( self::TABLE, $data );
    }


    public function update()
    {
        // TODO: Implement update() method.
    }

    public function delete(): bool
    {
        return Database::delete( self::TABLE, "id", $this->id );
    }

    public static function findByAgeGroup( SportAgeGroup $ageGroup ): array
    {

        $db = Database::instance();
        $statement = $db->prepare( "select * from age_group_students where age_group_id = ?" );
        $statement->execute( [ $ageGroup->id ] );

        /** @var self[] $results */
        $results = $statement->fetchAll( PDO::FETCH_CLASS, self::class );

        $output = [
            "age_group" => $ageGroup,
            "students" => [],
        ];

        if ( !empty( $results ) ) {
            foreach ( $results as $result ) {
                $result->student = Student::find( $result->student_id );
                $output["students"][] = $result;
            }
        }

        return $output;
    }

    public static function findByIds( int $age_group_id, int $student_id ): bool
    {

        $db = Database::instance();
        $statement = $db->prepare( "select * from age_group_students where age_group_id = ? and student_id = ? limit 1" );
        $statement->execute( [ $age_group_id, $student_id ] );

        $result = $statement->fetchObject( self::class );

        if ( !empty( $result ) ) return true;
        return false;

    }

}
