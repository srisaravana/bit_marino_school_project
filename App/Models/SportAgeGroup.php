<?php


namespace App\Models;


use App\Core\Database\Database;
use App\Models\Staff\Coach;
use App\Models\Staff\MasterInCharge;
use Exception;
use PDO;
use PDOException;

class SportAgeGroup implements IModel
{

    public const TABLE = 'sport_age_groups';

    public ?int $id, $sport_id, $master_in_charge_id, $coach_id, $year;
    public ?string $title;

    public ?MasterInCharge $master_in_charge;
    public ?Coach $coach;
    public ?Sport $sport;
    public ?SportPractice $sport_practice;


    /**
     * @param $array
     * @return self
     */
    public static function build( $array ): self
    {
        $object = new self();
        foreach ( $array as $key => $value ) {
            $object->$key = $value;
        }
        return $object;
    }


    /**
     * @param int $id
     * @return self|null
     */
    public static function find( int $id ): ?self
    {
        /** @var self $result */
        $result = Database::find( self::TABLE, $id, self::class );

        if ( !empty( $result ) ) {
            $result->sport = Sport::find( $result->sport_id );
            $result->master_in_charge = MasterInCharge::find( $result->master_in_charge_id );
            $result->coach = Coach::find( $result->coach_id );
            $result->sport_practice = SportPractice::findByAgeGroup( $result->id );
            return $result;
        }

        return null;

    }

    /**
     * @param int $limit
     * @param int $offset
     */
    public static function findAll( $limit = 1000, $offset = 0 )
    {
        // TODO: Implement findAll() method.
    }


    /**
     * @return int
     * @throws Exception
     */
    public function insert(): int
    {
        try {

            Database::instance()->beginTransaction();

            $data = [
                'sport_id' => $this->sport_id,
                'master_in_charge_id' => $this->master_in_charge_id,
                'coach_id' => $this->coach_id,
                'title' => $this->title,
                'year' => $this->year,
            ];

            $id = Database::insert( self::TABLE, $data );

            $ageGroup = SportAgeGroup::find( $id );
            if ( empty( $ageGroup ) ) throw new Exception( 'Failed inserting age group' );
            /* now we create new sport practice for the given age group */
            $fields = [
                'sport_age_group_id' => $ageGroup->id,
            ];

            Database::insert( 'sport_practices', $fields );

            Database::instance()->commit();
            return $id;

        } catch ( PDOException $exception ) {
            Database::instance()->rollBack();
            return -1;

        } catch ( Exception $exception ) {
            throw new Exception( $exception );
        }

    }

    public function update(): bool
    {
        $data = [
            'sport_id' => $this->sport_id,
            'master_in_charge_id' => $this->master_in_charge_id,
            'coach_id' => $this->coach,
            'title' => $this->title,
        ];

        return Database::update( self::TABLE, $data, [ 'id' => $this->id ] );
    }

    public function delete(): bool
    {
        return Database::delete( self::TABLE, 'id', $this->id );
    }

    /**
     * @param Sport $sport
     * @return self[]
     */
    public static function findBySport( Sport $sport ): array
    {
        $db = Database::instance();
        $statement = $db->prepare( 'select * from sport_age_groups where sport_id = ? order by year desc' );
        $statement->execute( [ $sport->id ] );

        /** @var self[] $results */
        $results = $statement->fetchAll( PDO::FETCH_CLASS, self::class );

        if ( !empty( $results ) ) {

            $sport = Sport::find( $sport->id );

            $output = [];

            foreach ( $results as $result ) {
                $result->sport = $sport;
                $result->master_in_charge = MasterInCharge::find( $result->master_in_charge_id );
                $result->coach = Coach::find( $result->coach_id );

                $output[] = $result;
            }

            return $output;
        }
        return [];

    }


}
