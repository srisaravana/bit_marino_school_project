<?php

namespace App\Models;

use App\Core\Database\Database;
use PDO;

class AgeGroupAchievement implements IModel
{

    private const TABLE = 'age_group_achievements';

    public ?int $id, $age_group_id, $game_id;
    public ?string $outcome, $tournament_date, $tournament_name, $game_data;

    public ?SportAgeGroup $sport_age_group;

    /**
     *
     * @param $array
     * @return static
     */
    public static function build( $array ): self
    {
        $object = new self();
        foreach ( $array as $key => $value ) {
            $object->$key = $value;
        }
        return $object;
    }

    /**
     * @param int $id
     * @return self|null
     */
    public static function find( int $id ): ?self
    {
        /** @var self $result */
        $result = Database::find( self::TABLE, $id, self::class );

        if ( !empty( $result ) ) {
            $result->sport_age_group = SportAgeGroup::find( $result->age_group_id );
            return $result;
        }

        return null;
    }

    public static function findAll( $limit = 1000, $offset = 0 )
    {
        // TODO: Implement findAll() method.
    }

    public function insert(): int
    {

        $ageGroup = SportAgeGroup::find( $this->age_group_id );

        $data = [
            'age_group_id' => $this->age_group_id,
            'outcome' => $this->outcome,
            'sport_id' => $ageGroup->sport->id,
            'tournament_date' => $this->tournament_date,
            'tournament_name' => $this->tournament_name,
            'game_data' => $this->game_data,
        ];

        return Database::insert( self::TABLE, $data );

    }

    public function update(): bool
    {
        $data = [
            'outcome' => $this->outcome,
            'tournament_date' => $this->tournament_date,
            'tournament_name' => $this->tournament_name,
            'game_data' => $this->game_data,
        ];

        return Database::update( self::TABLE, $data, [ 'id' => $this->id ] );
    }

    public function delete(): bool
    {
        return Database::delete( self::TABLE, 'id', $this->id );
    }


    /**
     * @param int $id
     * @return AgeGroupAchievement[]
     */
    public static function findBySportAgeGroup( int $id ): array
    {

        $db = Database::instance();
        $statement = $db->prepare( 'select * from age_group_achievements where age_group_id=?' );
        $statement->execute( [ $id ] );

        /** @var self[] $results */
        $results = $statement->fetchAll( PDO::FETCH_CLASS, self::class );

        if ( !empty( $results ) ) {
            return $results;
        }

        return [];
    }

}
