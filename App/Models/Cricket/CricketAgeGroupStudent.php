<?php

namespace App\Models\Cricket;

use App\Core\Database\Database;
use App\Models\IModel;
use App\Models\Student;
use PDO;

class CricketAgeGroupStudent implements IModel
{

    private const TABLE = 'cricket_age_group_students';

    public ?int $id, $cricket_age_group_id, $student_id;

    public ?Student $student;
    public ?CricketAgeGroup $cricket_age_group;

    public static function build( $array ): self
    {
        $object = new self();
        foreach ( $array as $key => $value ) {
            $object->$key = $value;
        }
        return $object;
    }

    /**
     * @param int $id
     * @return self|null
     */
    public static function find( int $id ): ?self
    {
        /** @var self $result */
        $result = Database::find( self::TABLE, $id, self::class );

        if ( !empty( $result ) ) {
            $result->student = Student::find( $result->student_id );
            $result->cricket_age_group = CricketAgeGroup::find( $result->student_id );

            return $result;
        }

        return null;

    }


    public static function findAll( $limit = 1000, $offset = 0 )
    {
        // TODO: Implement findAll() method.
    }


    public function insert(): int
    {
        $data = [
            'cricket_age_group_id' => $this->cricket_age_group_id,
            'student_id' => $this->student_id,
        ];

        return Database::insert( self::TABLE, $data );
    }


    public function update()
    {
        // TODO: Implement update() method.
    }


    public function delete(): bool
    {
        return Database::delete( self::TABLE, 'id', $this->id );
    }


    /**
     * @param CricketAgeGroup $cricketAgeGroup
     * @return self[]
     */
    public static function findByCricketAgeGroup( CricketAgeGroup $cricketAgeGroup ): array
    {
        $db = Database::instance();
        $statement = $db->prepare( 'select * from cricket_age_group_students where cricket_age_group_id = ?' );
        $statement->execute( [ $cricketAgeGroup->id ] );

        /** @var self[] $results */
        $results = $statement->fetchAll( PDO::FETCH_CLASS, self::class );

        if ( !empty( $results ) ) {

            foreach ( $results as $ageGroupStudent ) {
                $ageGroupStudent->student = Student::find( $ageGroupStudent->student_id );
                $ageGroupStudent->cricket_age_group = CricketAgeGroup::find( $ageGroupStudent->cricket_age_group_id );
            }
            return $results;
        }

        return [];
    }

}
