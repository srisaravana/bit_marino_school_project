import {CricketRoutes} from '@src/router/groups/cricket';
import Vue from 'vue';
import VueRouter from 'vue-router';

import store from '../store/index';
import Login from '../views/Login';
import Page404 from '../views/pages/Page404';
import {PagesRoutes} from './groups/pages';
import {SportsRoutes} from './groups/sports';
import {StaffRoutes} from './groups/staff';
import {StudentsRoutes} from './groups/students';

import {UserRoutes} from './groups/users';

Vue.use( VueRouter );


const routes = [
    {
        path: '/login',
        name: 'Login',
        component: Login,
    },

    ...PagesRoutes,
    ...UserRoutes,
    ...StudentsRoutes,
    ...StaffRoutes,
    ...SportsRoutes,
    ...CricketRoutes,

    {
        path: '*',
        name: 'Page404',
        component: Page404,
    },
];


const router = new VueRouter( {
    routes: routes,
} );


/**
 * To make sure only authenticated pages can be viewed if logged in
 * otherwise redirect to login page
 */
router.beforeEach( ( to, from, next ) => {
    const userType = store.getters.getUserType;
    const isLoggedIn = store.getters.getLoginStatus;


    if ( to.matched.some( record => record.meta.requiresAuth ) ) {

        if ( to.meta.hasOwnProperty( 'hasAccess' ) ) {
            if ( !to.meta.hasAccess.includes( userType ) ) {
                next( '/login' );
            }
        }

        if ( isLoggedIn ) {
            next();
        } else {
            next( '/login' );
        }

    } else {
        next();
    }
} );


export default router;
