import CricketAgeGroupPage from '@src/views/cricket/CricketAgeGroupPage';
import CricketPage from '@src/views/cricket/CricketPage';

export const CricketRoutes = [
    {
        path: '/cricket',
        name: 'cricketPage',
        component: CricketPage,
    },
    {
        path: '/cricket/age-group/:ageGroupId',
        name: 'cricketAgeGroupPage',
        component: CricketAgeGroupPage,
    },
];
