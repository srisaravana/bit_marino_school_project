import SportsPracticeDetailsPage from '@src/views/public/sports/SportsPracticeDetailsPage';
import AgeGroupDetailsPage from '@src/views/sports/age_groups/AgeGroupDetailsPage';
import ManageSportsPage from '@src/views/sports/ManageSportsPage';
import SportDetailsPage from '@src/views/sports/SportDetailsPage';

export const SportsRoutes = [
    {
        path: '/sports',
        component: ManageSportsPage,
        meta: {
            requiresAuth: true,
            hasAccess: ['ADMIN', 'MANAGER'],
        },
    },
    {
        path: '/sports/:id',
        name: 'editSport',
        component: SportDetailsPage,
        meta: {
            requiresAuth: true,
            hasAccess: ['ADMIN', 'MANAGER'],
        },
    },
    {
        path: '/sports/:sportId/age-group/:ageGroupId',
        name: 'ageGroupDetails',
        component: AgeGroupDetailsPage,
        meta: {
            requiresAuth: true,
            hasAccess: ['ADMIN', 'MANAGER'],
        },
    },
    {
        path: '/public/practice/:ageGroupId',
        name: 'publicSportPracticeDetails',
        component: SportsPracticeDetailsPage,
        meta: {},
    },
];
