import EditCoachPage from '@src/views/staff/coaches/EditCoachPage';
import ManageCoachesPage from '@src/views/staff/coaches/ManageCoachesPage';
import EditHpeTeacherPage from '@src/views/staff/hpe_teachers/EditHpeTeacherPage';
import ManageHpeTeachersPage from '@src/views/staff/hpe_teachers/ManageHpeTeachersPage';
import EditMasterInChargePage from '@src/views/staff/masters_in_charge/EditMasterInChargePage';
import ManageMastersInChargePage from '@src/views/staff/masters_in_charge/ManageMastersInChargePage';
import ManageStaff from '../../views/staff/ManageStaff';

export const StaffRoutes = [

    /* Staff */
    {
        path: '/staff',
        name: 'manageStaff',
        component: ManageStaff,
        meta: {
            requiresAuth: true,
            hasAccess: ['ADMIN', 'MANAGER'],
        },
    },
    /* HPE teachers */
    {
        path: '/staff/hpe-teachers',
        name: 'manageHpeTeachers',
        component: ManageHpeTeachersPage,
        meta: {
            requiresAuth: true,
            hasAccess: ['ADMIN', 'MANAGER'],
        },
    },
    {
        path: '/staff/hpe-teachers/edit/:id',
        name: 'editHpeTeacher',
        component: EditHpeTeacherPage,
        meta: {
            requiresAuth: true,
            hasAccess: ['ADMIN', 'MANAGER'],
        },
    },

    /* Masters in-charge */
    {
        path: '/staff/masters-in-charge',
        name: 'manageMastersInCharge',
        component: ManageMastersInChargePage,
        meta: {
            requiresAuth: true,
            hasAccess: ['ADMIN', 'MANAGER'],
        },
    },
    {
        path: '/staff/masters-in-charge/edit/:id',
        name: 'editMasterInCharge',
        component: EditMasterInChargePage,
        meta: {
            requiresAuth: true,
            hasAccess: ['ADMIN', 'MANAGER'],
        },
    },

    /* coaches */
    {
        path: '/staff/coaches',
        name: 'manageCoaches',
        component: ManageCoachesPage,
        meta: {
            requiresAuth: true,
            hasAccess: ['ADMIN', 'MANAGER'],
        },
    },
    {
        path: '/staff/coaches/edit/:id',
        name: 'editCoach',
        component: EditCoachPage,
        meta: {
            requiresAuth: true,
            hasAccess: ['ADMIN', 'MANAGER'],
        },
    },

];
