import axios from 'axios';


export const ageGroupsStore = {

    state: function () {

        return {

            selectedAgeGroup: {},
            ageGroupsList: [],

            ageGroupStudentsList: {
                age_group: {},
                students: [],
            },

        };
    },


    getters: {

        getSelectedAgeGroup( state ) {
            return state.selectedAgeGroup;
        },

        getAgeGroupsList( state ) {
            return state.ageGroupsList;
        },

        getAgeGroupStudentsList( state ) {
            return state.ageGroupStudentsList;
        },

    },


    actions: {

        async ageGroups_fetchBySport( context, sportId ) {


            const response = await axios.get( `sport-age-groups/by-sport.php?id=${ sportId }` );
            context.state.ageGroupsList = response.data.payload;

        }, /* fetch all */

        async ageGroups_fetch( context, id ) {

            const response = await axios.get( `sport-age-groups/get.php?id=${ id }` );
            context.state.selectedAgeGroup = response.data.payload.data;

        }, /* fetch */

        async ageGroups_create( context, params ) {


            await axios.post( 'sport-age-groups/create.php', params );


        }, /* create */

        async ageGroups_update( context, params ) {


            await axios.post( 'sport-age-groups/update.php', params );


        }, /* update */

        /* ---------------------------------------------------------------------------------- */

        /*
        * Actions for age group students
        * fetch students associated with the given age group
        * add new student to an age group
        * */

        async ageGroups_fetchGroupStudents( context, ageGroupId ) {
            const response = await axios.get( `sport-age-groups/get-students.php?id=${ ageGroupId }` );
            context.state.ageGroupStudentsList = response.data.payload;
        },

        async ageGroups_addAgeGroupStudent( context, params ) {
            await axios.post( 'sport-age-groups/add-student.php', params );
        },

        async ageGroups_removeAgeGroupStudent( context, id ) {
            await axios.post( 'sport-age-groups/remove-student.php', { id: id } );
        },
    },


};
