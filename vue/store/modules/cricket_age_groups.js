import axios from 'axios';

export const cricketAgeGroupsStore = {

    namespaced: true,

    state() {
        return {

            /** @type {CricketAgeGroup[]} */
            ageGroupsList: [],

            /** @type {CricketAgeGroup} */
            ageGroup: {},

            /** @type {CricketAgeGroupStudent[]} */
            cricketAgeGroupStudentsList: [],



        };
    },

    getters: {

        getAgeGroupsList( state ) {
            return state.ageGroupsList;
        },

        getAgeGroup( state ) {
            return state.ageGroup;
        },

        getStudentsListInAgeGroup( state ) {
            return state.cricketAgeGroupStudentsList;
        },



    },

    actions: {

        async fetchAll( context ) {

            const response = await axios.get( 'cricket-age-groups/all.php' );
            context.state.ageGroupsList = response.data.payload;
        },

        async create( context, params ) {
            await axios.post( 'cricket-age-groups/create.php', params );
        },

        async fetch( context, ageGroupId ) {
            const response = await axios.post( 'cricket-age-groups/get.php', { id: ageGroupId } );
            context.state.ageGroup = response.data.payload.data;
        },

        async update( context, params ) {
            await axios.post( 'cricket-age-groups/update.php', params );
        },

        async addStudent( context, params ) {
            await axios.post( 'cricket-age-groups/add-student.php', params );
        },

        async removeStudent( context, id ) {
            await axios.post( 'cricket-age-groups/remove-student.php', { id: id } );
        },

        async fetchAllAgeGroupStudents( context, ageGroupId ) {
            const response = await axios.post( 'cricket-age-groups/all-students.php', { id: ageGroupId } );
            context.state.cricketAgeGroupStudentsList = response.data.payload;
        },


        /*
        * ---------------------------
        * Cricket Performance Actions
        * ---------------------------
        * */




    },

};
