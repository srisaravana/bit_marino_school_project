import axios from 'axios';

export const cricketAgeGroup2DaysPerformanceStore = {
    namespaced: true,

    state() {
        return {
            /** @type {CricketAgeGroupPerformance[]} */
            ageGroup2DaysPerformancesList: [],

            /** @type {CricketAgeGroupPerformance} */
            ageGroup2DaysPerformance: {},

            /** @type {CricketGameStats} */
            stats2Days: {},
        };
    },

    getters: {
        getAgeGroup2DaysPerformancesList( state ) {
            return state.ageGroup2DaysPerformancesList;
        },

        getAgeGroup2DaysPerformance( state ) {
            return state.ageGroup2DaysPerformance;
        },

        get2DaysStats( state ) {
            return state.stats2Days;
        },
    },

    actions: {
        /**
         * Add a new performance record for the age group
         * @param context
         * @param params - age_group_id, tournament_name, match_date,
         *               - ground, a_runs, b_runs, a_wickets, b_wickets,
         *               - b_team_name, result
         */
        async add2DaysPerformance( context, params ) {
            await axios.post( 'cricket-age-groups/add-performance-2days.php', params );
        },

        /**
         * Remove the performance record from the age group
         * @param context
         * @param id
         */
        async remove2DaysPerformance( context, id ) {
            await axios.post( 'cricket-age-groups/remove-performance-2days.php', { id: id } );
        },

        /**
         * Fetch all one day performance records for the given age group
         * @param context
         * @param ageGroupId
         */
        async fetchAll2DaysPerformances( context, ageGroupId ) {
            const response = await axios.post( 'cricket-age-groups/all-performances-2days.php', { id: ageGroupId } );
            context.state.ageGroup2DaysPerformancesList = response.data.payload;
        },

        /**
         * Fetch selected one day performance details
         * @param context
         * @param performanceId
         */
        async fetch2DaysPerformance( context, performanceId ) {
            const response = await axios.post( 'cricket-age-groups/get-performance-2days.php', { id: performanceId } );
            context.state.ageGroup2DaysPerformance = response.data.payload.data;
        },


        /**
         * Get one day play statistics for the age group
         * @param context
         * @param ageGroupId
         */
        async fetch2DaysStats( context, ageGroupId ) {
            const response = await axios.post( 'cricket-age-groups/get-stats-2days.php', { id: ageGroupId } );
            context.state.stats2Days = response.data.payload;
        },

    },

};
