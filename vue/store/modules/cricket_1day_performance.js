import axios from 'axios';

export const cricketAgeGroup1DayPerformanceStore = {
    namespaced: true,

    state() {
        return {
            /** @type {CricketAgeGroupPerformance[]} */
            ageGroupPerformancesList: [],

            /** @type {CricketAgeGroupPerformance} */
            ageGroupPerformance: {},

            /** @type {CricketGameStats} */
            stats: {},
        };
    },

    getters: {
        getAgeGroupPerformancesList( state ) {
            return state.ageGroupPerformancesList;
        },

        getAgeGroupPerformance( state ) {
            return state.ageGroupPerformance;
        },

        getStats( state ) {
            return state.stats;
        },
    },

    actions: {
        /**
         * Add a new performance record for the age group
         * @param context
         * @param params - age_group_id, tournament_name, match_date,
         *               - ground, a_runs, b_runs, a_wickets, b_wickets,
         *               - b_team_name, result
         */
        async addPerformance( context, params ) {
            await axios.post( 'cricket-age-groups/add-performance-1day.php', params );
        },

        /**
         * Remove the performance record from the age group
         * @param context
         * @param id
         */
        async removePerformance( context, id ) {
            await axios.post( 'cricket-age-groups/remove-performance-1day.php', { id: id } );
        },

        /**
         * Update the performance record from the age group
         * @param context
         * @param params
         */
        async updatePerformance( context, params ) {
            await axios.post( 'cricket-age-groups/update-performance-1day.php', params );
        },

        /**
         * Fetch all one day performance records for the given age group
         * @param context
         * @param ageGroupId
         */
        async fetchAllPerformances( context, ageGroupId ) {
            const response = await axios.post( 'cricket-age-groups/all-performances-1day.php', { id: ageGroupId } );
            context.state.ageGroupPerformancesList = response.data.payload;
        },

        /**
         * Fetch selected one day performance details
         * @param context
         * @param performanceId
         */
        async fetchPerformance( context, performanceId ) {
            const response = await axios.post( 'cricket-age-groups/get-performance-1day.php', { id: performanceId } );
            context.state.ageGroupPerformance = response.data.payload.data;
        },


        /**
         * Get one day play statistics for the age group
         * @param context
         * @param ageGroupId
         * @return {Promise<void>}
         */
        async fetchStats( context, ageGroupId ) {
            const response = await axios.post( 'cricket-age-groups/get-stats-1day.php', { id: ageGroupId } );
            context.state.stats = response.data.payload;
        },

    },

};
