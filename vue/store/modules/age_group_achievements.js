export const ageGroupAchievementsStore = {

    state() {
        return {

            /** @type {AgeGroupAchievement[]} */
            ageGroupAchievementsList: [],

            /** @type {AgeGroupAchievement} */
            selectedAgeGroupAchievement: {},

            outcomes: {
                'UNSPECIFIED': 'Unspecified',
                '1ST': 'First',
                '2ND': 'First runner up',
                '3RD': 'Second runner up',
                'TIE': 'Tie',
                'UNQUALIFIED': 'Unqualified',
            },

        };
    },

    getters: {
        getAchievementOutcome( state ) {
            return state.outcomes;
        },
    },

    actions: {},

};
