import axios from 'axios';

export const sportsStore = {

    state() {
        return {

            /** @type {Sport} */
            selectedSport: {},
            /** @type {Sport[]} */
            sportsList: [],

        };

    },


    getters: {

        getSelectedSport( state ) { return state.selectedSport; },
        getSportsList( state ) { return state.sportsList; },

    },


    actions: {

        async sports_fetchAll( context ) {

            const response = await axios.get( `sports/all.php` );
            context.state.sportsList = response.data.payload.sports;

        }, /* fetch all */

        async sports_fetch( context, id ) {
            const response = await axios.get( `sports/get.php?id=${ id }` );
            context.state.selectedSport = response.data.payload.data;
        }, /* fetch */

        async sports_create( context, params ) {
            await axios.post( 'sports/create.php', params );
        }, /* create */

        async sports_update( context, params ) {
            await axios.post( 'sports/update.php', params );
        }, /* update */

    },

};
