import axios from 'axios';

export const hpeTeachersStore = {

    namespaced: true,

    state: function () {
        return {
            /** @type {HpeTeacher[]} */
            hpeTeachersList: [],

            /** @type {HpeTeacher} */
            selectedHpeTeacher: {},
        };
    },

    getters: {
        /* HPE teachers */
        getHpeTeachersList( state ) {
            return state.hpeTeachersList;
        },

        getSelectedHpeTeacher( state ) {
            return state.selectedHpeTeacher;
        },
    },

    actions: {
        async fetchAll( context ) {

            try {

                const response = await axios.get( `staff/hpe-teachers/all.php` );
                context.state.hpeTeachersList = response.data.payload;
            } catch ( e ) {
                throw e;
            }
        }, /* fetch all */

        async fetch( context, id ) {
            try {
                const response = await axios.get( `staff/hpe-teachers/get.php?id=${ id }` );
                context.state.selectedHpeTeacher = response.data.payload.data;
            } catch ( e ) {
                throw e;
            }
        }, /* fetch */

        async create( context, params ) {
            try {
                await axios.post( 'staff/hpe-teachers/create.php', params );
            } catch ( e ) {
                throw e;
            }
        }, /* create */

        async update( context, params ) {
            try {
                await axios.post( 'staff/hpe-teachers/update.php', params );
            } catch ( e ) {
                throw e;
            }
        }, /* update */

        async uploadProfilePic( context, params ) {
            try {

                let formData = new FormData();
                formData.append( 'id', params.id );
                formData.append( 'profile_pic', params.profile_pic );

                await axios.post( 'staff/hpe-teachers/upload-profile-pic.php', formData, {
                    'Content-Type': 'multipart/form-data',
                } );
            } catch ( e ) {
                throw e;
            }
        }, /* upload profile pic */

    },

};
