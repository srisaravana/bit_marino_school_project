import axios from 'axios';

export const coachesStore = {

    namespaced: true,

    state() {

        return {

            /** @type {Coach[]} */
            coachesList: [],
            /** @type {Coach} */
            selectedCoach: {},

        };

    },

    getters: {


        /* Coaches */
        getCoachesList( state ) {
            return state.coachesList;
        },
        getSelectedCoach( state ) {
            return state.selectedCoach;
        },


    },

    mutations: {},

    actions: {


        async fetchAll( context ) {

            const response = await axios.get( `staff/coaches/all.php` );
            context.state.coachesList = response.data.payload;

        }, /* fetch all */

        async fetch( context, id ) {
            const response = await axios.get( `staff/coaches/get.php?id=${ id }` );
            context.state.selectedCoach = response.data.payload.data;
        }, /* fetch */

        async create( context, params ) {
            await axios.post( 'staff/coaches//create.php', params );
        }, /* create */

        async update( context, params ) {
            await axios.post( 'staff/coaches/update.php', params );
        }, /* update */

    },

};
