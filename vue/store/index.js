import {ageGroupAchievementsStore} from '@src/store/modules/age_group_achievements';
import {ageGroupsStore} from '@src/store/modules/age_groups';
import {authStore} from '@src/store/modules/auth';
import {coachesStore} from '@src/store/modules/coaches';
import {cricketAgeGroup1DayPerformanceStore} from '@src/store/modules/cricket_1day_performance';
import {cricketAgeGroup2DaysPerformanceStore} from '@src/store/modules/cricket_2days_performance';
import {cricketAgeGroupsStore} from '@src/store/modules/cricket_age_groups';
import {cricketAgeGroupT20PerformanceStore} from '@src/store/modules/cricket_t20_performance.js';
import {hpeTeachersStore} from '@src/store/modules/hpe_teachers';
import {masterInChargeStore} from '@src/store/modules/masters_in_charge';
import {sportPracticesStore} from '@src/store/modules/sport_practices';
import {sportsStore} from '@src/store/modules/sports';
import {studentsStore} from '@src/store/modules/students';
import {usersStore} from '@src/store/modules/users';

import Vue from 'vue';
import Vuex from 'vuex';


Vue.use( Vuex );

export default new Vuex.Store( {
    namespaced: true,
    state: {},
    mutations: {},
    actions: {},
    modules: {
        auth: authStore,
        users: usersStore,
        students: studentsStore,

        coaches: coachesStore,
        hpeTeachers: hpeTeachersStore,
        mastersInCharge: masterInChargeStore,

        sports: sportsStore,
        ageGroups: ageGroupsStore,
        practices: sportPracticesStore,
        achievements: ageGroupAchievementsStore,
        cricketAgeGroups: cricketAgeGroupsStore,
        cricket1DayPerf: cricketAgeGroup1DayPerformanceStore,
        cricket2DaysPerf: cricketAgeGroup2DaysPerformanceStore,
        cricketT20Perf: cricketAgeGroupT20PerformanceStore,
    },

} );
