/**
 * @typedef {Object} User
 * @property {number} id
 * @property {string} username
 * @property {string} full_name
 * @property {string} password
 * @property {string} email
 * @property {string} role
 */


/**
 * @typedef {Object} Student
 * @property {number} id
 * @property {string} full_name
 * @property {string} dob
 * @property {string} house
 * @property {string} date_of_admission
 * @property {string} bc_number
 * @property {string} email
 * @property {string} address
 * @property {string} guardian_name
 * @property {string} contact_number
 * @property {string} grade
 * @property {string} admission_number
 * @property {string} profile_pic
 */


/**
 * @typedef {Object} Coach
 * @property {number} id
 * @property {string} full_name
 * @property {string} contact_number
 * @property {string} email
 * @property {string} address
 * @property {string} coaching_license
 */


/**
 * @typedef {Object} HpeTeacher
 * @property {number} id
 * @property {string} full_name
 * @property {string} contact_number
 * @property {string} email
 * @property {string} address
 * @property {string} profile_pic
 */


/**
 * @typedef {Object} MasterInCharge
 * @property {number} id
 * @property {string} full_name
 * @property {string} contact_number
 * @property {string} email
 * @property {string} address
 */


/**
 * @typedef {Object} Sport
 * @property {number} id
 * @property {number} hpe_teacher_id
 * @property {number} master_in_charge_id
 * @property {MasterInCharge} master_in_charge
 * @property {HpeTeacher} hpe_teacher
 * @property {string} title
 */


/**
 * @typedef {Object} SportAgeGroup
 * @property {number} id
 * @property {number} sport_id
 * @property {number} master_in_charge_id
 * @property {number} coach_id
 * @property {string} title
 * @property {Number} year
 * @property {Sport} sport
 * @property {MasterInCharge} master_in_charge
 * @property {Coach} coach
 * @property {SportPractice} sport_practice
 */


/**
 * @typedef {Object} SportPractice
 * @property {number} id
 * @property {number} sport_age_group_id
 * @property {string} place
 * @property {string} monday
 * @property {string} tuesday
 * @property {string} wednesday
 * @property {string} thursday
 * @property {string} friday
 * @property {string} saturday
 * @property {string} sunday
 * @property {SportAgeGroup} sport_age_group
 */


/**
 * @typedef {Object} AgeGroupStudent
 * @property {number} age_group_id
 * @property {number} id
 * @property {number} student_id
 * @property {Student} student
 */

/**
 * @typedef {Object} AgeGroupAchievement
 * @property {number} id
 * @property {number} age_group_id
 * @property {string} tournament_date
 * @property {string} tournament_name
 * @property {string} outcome
 * @property {string} game_data
 * @property {SportAgeGroup} sport_age_group
 */


/*
* ----------------------------------------------------------------------
* Cricket types
* ----------------------------------------------------------------------
* */


/**
 * @typedef {Object} CricketAgeGroup
 * @property {number} id
 * @property {number} master_in_charge_id
 * @property {number} coach_id
 * @property {string} title
 * @property {Number} year
 * @property {MasterInCharge} master_in_charge
 * @property {Coach} coach
 */

/**
 * @typedef {Object} CricketAgeGroupStudent
 * @property {number} id
 * @property {number} cricket_age_group_id
 * @property {number} student_id
 * @property {Student} student
 * @property {CricketAgeGroup} cricket_age_group
 */


/**
 * @typedef {Object} CricketAgeGroupPerformance
 * @property {number} id
 * @property {number} age_group_id
 * @property {string} match_date
 * @property {string} tournament_name
 * @property {string} ground
 * @property {string} match_type
 * @property {string} b_team_name
 * @property {number} a_runs
 * @property {number} a_runs_1
 * @property {number} a_runs_2
 * @property {number} b_runs
 * @property {number} b_runs_1
 * @property {number} b_runs_2
 * @property {number} a_wickets
 * @property {number} a_wickets_1
 * @property {number} a_wickets_2
 * @property {number} b_wickets
 * @property {number} b_wickets_1
 * @property {number} b_wickets_2
 * @property {string} result
 * @property {CricketAgeGroup} age_group
 */

/**
 * @typedef {Object} CricketStatsMatches
 * @property {number} win
 * @property {number} loss
 * @property {number} tie
 * @property {number} draw
 * @property {number} no
 */


/**
 * @typedef {Object} CricketStatsGamesRuns
 * @property {Array} games_titles
 * @property {Array} team_a_runs_1
 * @property {Array} team_a_runs_2
 * @property {Array} team_b_runs_1
 * @property {Array} team_b_runs_2
 */


/**
 * @typedef {Object} CricketGameStats
 * @property {CricketStatsMatches} matches
 * @property {CricketStatsGamesRuns} games_runs
 */
