<?php

declare( strict_types=1 );

use App\Core\Http\Auth;
use App\Core\Http\JSONResponse;
use App\Core\Http\Request;
use App\Models\Sport;
use App\Models\SportAgeGroup;

require_once "../../../bootstrap.php";

try {

    /*
     * Authenticate for incoming auth key
     * if no valid key is present, will return 401
     * */
    Auth::authenticate();


    $id = Request::getAsInteger( "id", true );

    $sport = Sport::find( $id );

    if ( empty( $sport ) ) throw new Exception( "Invalid sport id" );

    $ageGroups = SportAgeGroup::findBySport( $sport );

    JSONResponse::validResponse( $ageGroups );
    return;


} catch ( Exception $exception ) {
    JSONResponse::exceptionResponse( $exception );
}
