<?php

declare( strict_types=1 );

use App\Core\Http\Auth;
use App\Core\Http\JSONResponse;
use App\Core\Http\Request;
use App\Models\AgeGroupStudent;
use App\Models\SportAgeGroup;

require_once "../../../bootstrap.php";

try {

    /*
     * Authenticate for incoming auth key
     * if no valid key is present, will return 401
     * */
//    Auth::authenticate();


    $id = Request::getAsInteger( "id", true );


    $ageGroup = SportAgeGroup::find( $id );

    if ( empty( $ageGroup ) ) throw new Exception( "Invalid age group" );


    $ageGroupStudents = AgeGroupStudent::findByAgeGroup( $ageGroup );
    JSONResponse::validResponse( $ageGroupStudents );
    return;


} catch ( Exception $exception ) {
    JSONResponse::exceptionResponse( $exception );
}
