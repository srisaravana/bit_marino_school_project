<?php

declare(strict_types=1);

use App\Core\Http\Auth;
use App\Core\Http\JSONResponse;
use App\Core\Http\Request;
use App\Models\Staff\Coach;

require_once "../../../../bootstrap.php";

try {

    /*
     * Authenticate for incoming auth key
     * if no valid key is present, will return 401
     * */
    Auth::authenticate();

    $id = Request::getAsInteger("id", true);


    $coach = Coach::find($id);

    if (is_null($coach)) throw new Exception("Invalid coach");

    JSONResponse::validResponse($coach);
    return;


} catch (Exception $exception) {
    JSONResponse::exceptionResponse($exception);
}
