<?php

declare( strict_types=1 );

use App\Core\Http\Auth;
use App\Core\Http\JSONResponse;
use App\Core\Http\Request;
use App\Models\Cricket\CricketAgeGroupStudent;

require_once '../../../bootstrap.php';

try {

    /*
     * Authenticate for incoming auth key
     * if no valid key is present, will return 401
     * */
    Auth::authenticate();


    $fields = [
        'id' => Request::getAsInteger( 'id', true ),
    ];

    $cricketAgeGroupStudent = CricketAgeGroupStudent::build( $fields );

    $result = $cricketAgeGroupStudent->delete();

    if ( $result ) {
        JSONResponse::validResponse();
        return;
    }

    throw new Exception( 'Failed to remove student' );

} catch ( Exception $exception ) {
    JSONResponse::exceptionResponse( $exception );
}
