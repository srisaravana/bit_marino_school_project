<?php

declare(strict_types=1);

use App\Core\Http\Auth;
use App\Core\Http\JSONResponse;
use App\Core\Http\Request;
use App\Models\Student;

require_once "../../../bootstrap.php";

try {

    /*
     * Authenticate for incoming auth key
     * if no valid key is present, will return 401
     * */
    Auth::authenticate();


    $fields = [
        "full_name" => Request::getAsString("full_name", true),
        "admission_number" => Request::getAsInteger("admission_number", true),
        "dob" => Request::getAsString("dob", true),
        "grade" => Request::getAsString("grade", true),
        "house" => Request::getAsString("house", true),
        "date_of_admission" => Request::getAsString("date_of_admission", true),
        "bc_number" => Request::getAsString("bc_number", true),
        "email" => Request::getAsString("email", true),
        "contact_number" => Request::getAsString("contact_number", true),
        "address" => Request::getAsString("address", true),
        "guardian_name" => Request::getAsString("guardian_name", true),
    ];


    $student = Student::build($fields);

    $result = $student->insert();

    if ($result) {

        $student = Student::find($result);

        JSONResponse::validResponse(["student" => $student]);
        return;
    }


} catch (Exception $exception) {
    JSONResponse::exceptionResponse($exception);
}
