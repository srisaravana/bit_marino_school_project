<?php

declare( strict_types=1 );

use App\Core\Http\Auth;
use App\Core\Http\JSONResponse;
use App\Core\Http\Request;
use App\Models\Student;

require_once "../../../bootstrap.php";

try {

    /*
     * Authenticate for incoming auth key
     * if no valid key is present, will return 401
     * */
    Auth::authenticate();

//    $fields = [
//        "limit" => Request::getAsInteger("limit"),
//        "offset" => Request::getAsInteger("offset"),
//    ];

    $query = Request::getAsString( "q" ) ?? '';

    $result = Student::search( $query );

    JSONResponse::validResponse( $result );
    return;

} catch ( Exception $exception ) {
    JSONResponse::exceptionResponse( $exception );
}
