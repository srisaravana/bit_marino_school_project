<?php

declare( strict_types=1 );

use App\Core\Http\Auth;
use App\Core\Http\JSONResponse;
use App\Core\Http\Request;
use App\Models\Student;

require_once "../../../bootstrap.php";

try {

    Auth::authenticate();

    $fields = [
        "id" => Request::getAsInteger( "id", true ),
    ];

    $student = Student::find( $fields["id"] );
    if ( empty( $student ) ) throw new Exception( "Invalid student" );


    if ( $student->removeProfilePicture() ) {
        JSONResponse::validResponse();
        return;
    }

    throw new Exception( "Failed to remove profile picture" );


} catch ( Exception $exception ) {
    JSONResponse::exceptionResponse( $exception );
}
