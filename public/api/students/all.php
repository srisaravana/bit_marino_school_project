<?php

declare(strict_types=1);

use App\Core\Http\Auth;
use App\Core\Http\JSONResponse;
use App\Core\Http\Request;
use App\Models\Student;

require_once "../../../bootstrap.php";

try {

    /*
     * Authenticate for incoming auth key
     * if no valid key is present, will return 401
     * */
    Auth::authenticate();

    $fields = [
        "limit" => Request::getAsInteger("limit"),
        "offset" => Request::getAsInteger("offset"),
    ];

    $students = [];

    if (is_null($fields["limit"]) || is_null($fields["offset"])) {
        $students = Student::findAll();
    } else {
        $students = Student::findAll($fields["limit"], $fields["offset"]);
    }

    $total = Student::getCount();

    JSONResponse::validResponse(["students" => $students, "total" => $total]);
    return;


} catch (Exception $exception) {
    JSONResponse::exceptionResponse($exception);
}
