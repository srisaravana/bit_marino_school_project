<?php

declare( strict_types=1 );

use App\Core\Http\Auth;
use App\Core\Http\JSONResponse;
use App\Core\Http\Request;
use App\Core\Resources\MimeTypes;
use App\Core\Services\ImageProcessor;
use App\Core\Services\Uploader;
use App\Core\Services\Storage;
use App\Models\Student;

require_once "../../../bootstrap.php";

try {

    /*
     * Authenticate for incoming auth key
     * if no valid key is present, will return 401
     * */
    Auth::authenticate();

    $fields = [
        "id" => Request::getAsInteger( "id", true ),
        "profile_pic" => Request::getFile( "profile_pic" ),
    ];

    $student = Student::find( $fields["id"] );
    if ( empty( $student ) ) throw new Exception( "Invalid student" );

    $uploader = new Uploader( $fields["profile_pic"], Storage::getMaxUploadSize(), "uploads/images/students", MimeTypes::IMAGE_MIMES );

    $processor = ImageProcessor::createImageObject( $uploader );
    $processor->fit( 200 )->save( "student_" . $student->id . ".jpg", 80, "jpg" );

    $path = $processor->getRelativePath();

    $student->profile_pic = $path;

    if ( $student->updateProfilePicture() ) {
        JSONResponse::validResponse();
        return;
    }

    throw new Exception( "Failed to upload profile picture" );


} catch ( Exception $exception ) {
    JSONResponse::exceptionResponse( $exception );
}
