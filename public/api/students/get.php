<?php

declare(strict_types=1);

use App\Core\Http\Auth;
use App\Core\Http\JSONResponse;
use App\Core\Http\Request;
use App\Models\Student;
use App\Models\User;

require_once "../../../bootstrap.php";

try {

    /*
     * Authenticate for incoming auth key
     * if no valid key is present, will return 401
     * */
    Auth::authenticate();


    $id = Request::getAsInteger("id", true);


    $student = Student::find($id);

    if (is_null($student)) throw new Exception("Invalid student");

    JSONResponse::validResponse($student);
    return;


} catch (Exception $exception) {
    JSONResponse::exceptionResponse($exception);
}
